
# node!
This is an example server using Node.js with Express and Nunjucks, alongside some Jest tests, and my typical ESLint+TypeScript combo.



## npm run watch
Run `nodemon` dev server based on `nodemon.json`



## npm run test
Run `jest` tests based on `jest.config.cjs`, which tests the TypeScript files and treats them as modules.



## ESLint
`.eslintrc.cjs` relies on `.eslint-global-config.cjs` in its JavaScript and TypeScript ruleset.

I went with this type of configuration during my "RIA GameStart (Game.inc/g)" project because I wanted to accommodate people that don't know/care for TypeScript, hence allowing TypeScript, and I didn't want to have to repeat the ruleset. Of course, I wrote everything and just used TypeScript, but oh well.

I don't actually know why the `.cjs` extension is required; I've used modules with TypeScript and ESLint as a `.js` file before, and I tried using `export default` instead of `module.exports`, but that didn't change anything.



## Files
These are some file-specific descriptors 



### package.json
This is configured to be as a module.



### tsconfig.json
This is set to compile the `src` directory. Invoking `tsc` emits to `build` directory, but this isn't the case otherwise.



### nodemon.jsom
`node --es-module-specifier-resolution=node --loader ts-node/esm ./src/server.ts`

This restarts the server whenever anything in the `src` directory changes, and it nicely just uses TypeScript instead of requiring a JS `build` folder.

Based on https://stackoverflow.com/a/66626333, I'm using this command instead of `ts-node`.

Additionally, `--es-module-specifier-resolution=node` (is required, because of some other issues when there *was* a file extension present, and) allows the ommission of file extensions with import syntax.