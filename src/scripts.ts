import mongodb, {MongoClient as dbconn} from "mongodb";

/**
 * "Helper" function just to save typing URL for dbconn
 *
 * @return {*} {(mongodb.MongoClient | string)}
 */
const makeDbConn = async (): Promise<mongodb.MongoClient> => {
	return dbconn.connect("mongodb://localhost:27017/node_todo");

	// dbconn.connect("mongodb://localhost:27017/node_todo", (err: mongodb.AnyError | undefined, client: mongodb.MongoClient | undefined) => {
	// 	// if (err) return console.error(err);
	// 	if (err) {
	// 		console.log("mongodb fail");
	// 		throw err;
	// 	}
	// 	console.log("Connected to Database");
	// });
};


export {
	makeDbConn,
};
