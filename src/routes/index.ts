// -----------------------------------------------------------------------------
// External
import express from "express";
// -----------------------------------------------------------------------------

// eslint-disable-next-line
const routerIndex = express.Router();


routerIndex.get("/", (req: express.Request, res: express.Response) => {
	const data = {
		layout: "base.njk",
		title: "Nunjucks example",
		user: "Curious User",
	};

	res.render("home.njk", data);
});

export default routerIndex;
