// -----------------------------------------------------------------------------
// External
import express from "express";
import nunjucks from "nunjucks";
// import {MongoClient} from "mongodb";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Internal
import {makeDbConn} from "./scripts";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Routes
import routerIndex from "./routes/index";
import routerApi from "./api/index";
// -----------------------------------------------------------------------------



// SECTION App Confing
const app = express();

nunjucks.configure("src/views", {
	autoescape: true,
	express: app,
});
// !SECTION


app.use("/", routerIndex);
app.use("/api/", routerApi);


app.get("/test", (req: express.Request, res: express.Response) => {
	res.status(200).send("Hello World!");
});

app.listen(3000, () => {
	console.log("Example app listening on port 3000!");
});

export default app;
